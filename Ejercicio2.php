<?php
//Iniciamos la conexión al servidor
include('conexion.php');
?>

<!DOCTYPE html>
<html>
<head>
	<title>Ejercicio 2</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="style.css">
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha256-pasqAKBDmFT4eHoN2ndd6lN370kFiGUFyTiUHWhU7k8=" crossorigin="anonymous"></script>
</head>
<body>
<div class="row">
	<div class="container">
		<!--Luego escupimos los datos de la siguiente manera-->
		<table class="table table-striped tabla" id="table">
		  <thead>
		    <tr>
		      <th scope="col">Día</th>
		      <th scope="col">Fecha y hora</th>
		      <th scope="col">Dolar</th>
		      <th scope="col">Euro</th>
		    </tr>
		  </thead>
		  <tbody>
		  	<?php
			
			$sql= "SELECT * from dolar";
			$result = mysqli_query($conectar,$sql);
			//print_r($result);

			while($mostrar=mysqli_fetch_array($result)){
				//print_r($mostrar);
			?>
		    <tr>
		      <th scope="row"><?php echo $mostrar['Dia'] ?>  </th>
		      <td><?php echo $mostrar['Fecha'] ?>  </td>
		      <td><?php echo $mostrar['Dolar'] ?>  </td>
		      <td><?php echo $mostrar['Euro'] ?>  </td>
		    </tr>
		    <?php 
			}
			?>
		  </tbody>
		</table>
	</div>	
</div>

</body>
</body>
</html>