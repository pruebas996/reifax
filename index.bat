<?php
$num = trim(fgets(fopen('php://stdin', 'r')));
while($num!=0){
  $raizPrima=$num;
  if ($num>1){
    if (!isPrime($num)){
      $raizPrima=raizPrima($num);
    } else {
      $raizPrima=$num;
    }
  } else{
    $raizPrima='none';
  }
  echo str_repeat(' ', 7-strlen($num)).$num.str_repeat(' ', 8-strlen($raizPrima)).$raizPrima."\n";
  $num = str_replace("\n", "", fgets('php://stdin'));
}
function raizPrima($num){
 $sw=true;
 $resp='none';
  while(strlen($num)>1 && $sw)  {
    $k=0;
    for($i=0;$i<strlen($num);$i++)
      $k=$k+$num[$i];
    if (!isPrime($k))
      $num=(string)$k;
    else {
      $resp=$k;
      $sw=false;
    }
  }
  return($resp);
}
function isPrime($num)
{
  $cont = 0;
  for($i = 1; $i <= $num; $i++){
    if($num % $i == 0)
      $cont++;
  }
  if($cont==2)
    return true;
  else
    return false;
} 
?>