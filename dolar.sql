-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 11-09-2019 a las 15:56:42
-- Versión del servidor: 10.1.38-MariaDB
-- Versión de PHP: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `dolar`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dolar`
--

CREATE TABLE `dolar` (
  `Dia` text COLLATE utf8_bin NOT NULL,
  `Fecha` varchar(100) COLLATE utf8_bin NOT NULL,
  `Dolar` varchar(15) COLLATE utf8_bin NOT NULL,
  `Euro` varchar(15) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `dolar`
--

INSERT INTO `dolar` (`Dia`, `Fecha`, `Dolar`, `Euro`) VALUES
('MiÃ©rcoles', 'Septiembre 11, 2019 09:42 AM', '22101.21', '24333.43'),
('MiÃ©rcoles', 'Septiembre 11, 2019 09:42 AM', '22101.21', '24333.43'),
('MiÃ©rcoles', 'Septiembre 11, 2019 09:42 AM', '22101.21', '24333.43'),
('MiÃ©rcoles', 'Septiembre 11, 2019 09:42 AM', '22101.21', '24333.43'),
('MiÃ©rcoles', 'Septiembre 11, 2019 09:42 AM', '22101.21', '24333.43'),
('MiÃ©rcoles', 'Septiembre 11, 2019 09:42 AM', '22101.21', '24333.43'),
('MiÃ©rcoles', 'Septiembre 11, 2019 09:42 AM', '22101.21', '24333.43'),
('MiÃ©rcoles', 'Septiembre 11, 2019 09:42 AM', '22101.21', '24333.43'),
('MiÃ©rcoles', 'Septiembre 11, 2019 09:42 AM', '22101.21', '24333.43'),
('MiÃ©rcoles', 'Septiembre 11, 2019 09:42 AM', '22101.21', '24333.43'),
('MiÃ©rcoles', 'Septiembre 11, 2019 09:42 AM', '22101.21', '24333.43');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
